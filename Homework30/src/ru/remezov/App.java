package ru.remezov;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a word.");
        String word = scanner.next();
        int indicator = 0;
        for (int i = 0; i < word.length(); i++) {
            int count = 0;
            for (int j = 0; j < word.length(); j++) {
                if (word.charAt(i) == word.charAt(j)) {
                    count++;
                }
            }
            if (count == 1) {
                System.out.printf("The first non-repeated letter is %s.", word.charAt(i));
                indicator++;
                break;
            }
        }
        if (indicator == 0) {
            System.out.println("All of the letters are repeated in the word.");
        }
    }
}
